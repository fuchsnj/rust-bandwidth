use BResult;
use client::{EmptyResponse, JsonResponse, Client};
use std::sync::{Arc, Mutex};
use call_event::CallEvent;
use bridge::Bridge;
use util;
use lazy::Lazy;
use lazy::Lazy::*;
use self::info::CallInfo;

pub struct Call{
	id: String,
	client: Client,
	data: Arc<Mutex<Data>> 
}

struct Data{
	active_time: Lazy<Option<String>>,
	bridge_id: Lazy<Option<String>>,
	callback_url: Lazy<Option<String>>,
	direction: Lazy<String>,
	from: Lazy<String>,
	recording_file_format: Lazy<Option<String>>,
	recording_enabled: Lazy<bool>,
	start_time: Lazy<String>,
	state: Lazy<String>,
	to: Lazy<String>,
	transcription_enabled: Lazy<bool>,
	display_name: Lazy<Option<String>>,
	preferred_id: Lazy<Option<String>>,
	withhold_caller_name: Lazy<Option<bool>>,
	withhold_caller_number: Lazy<Option<bool>>
}
impl Data{
	fn from_info(info: &CallInfo) -> BResult<Data>{
		Ok(Data{
			active_time: Available(info.activeTime.clone()),
			bridge_id: Available(match info.bridge{
				Some(ref url) => Some(try!(util::get_id_from_location_url(url))),
				None => None
			}),
			callback_url: Available(info.callbackUrl.clone()),
			direction: Available(info.direction.clone()),
			from: Available(info.from.clone()),
			recording_file_format: Available(info.recordingFileFormat.clone()),
			recording_enabled: Available(info.recordingEnabled),
			start_time: Available(info.startTime.clone()),
			state: Available(info.state.clone()),
			to: Available(info.to.clone()),
			transcription_enabled: Available(info.transcriptionEnabled),
			display_name: Available(info.displayName.clone()),
			preferred_id: Available(info.preferredId.clone()),
			withhold_caller_name: Available(info.withholdCallerName),
			withhold_caller_number: Available(info.withholdCallerNumber)
		})
	}
}

mod info{
	#![allow(non_snake_case)]
	#[derive(RustcDecodable)]
	pub struct CallInfo{
		pub id: String,
		pub activeTime: Option<String>,
		pub bridge: Option<String>,
		pub callbackUrl: Option<String>,
		pub direction: String,
		pub from: String,
		pub recordingFileFormat: Option<String>,
		pub recordingEnabled: bool,
		pub startTime: String,
		pub state: String,
		pub to: String,
		pub transcriptionEnabled: bool,
		pub displayName: Option<String>,
		pub preferredId: Option<String>,
		pub withholdCallerName: Option<bool>,
		pub withholdCallerNumber: Option<bool>
	}
}

pub struct Config{
	pub call_timeout: Option<u64>,
	pub callback_url: Option<String>,
	pub callback_timeout: Option<u64>,
	pub callback_http_method: Option<String>,
	pub fallback_url: Option<String>,
	pub bridge_id: Option<String>,
	pub conference_id: Option<String>,
	pub recording_enabled: Option<bool>,
	pub recording_max_duration: Option<u64>,
	pub transcription_enabled: Option<bool>,
	pub tag: Option<String>	
}
impl Config{
	pub fn new() -> Config{
		Config{
			call_timeout: None,
			callback_url: None,
			callback_timeout: None,
			callback_http_method: None,
			fallback_url: None,
			bridge_id: None,
			conference_id: None,
			recording_enabled: None,
			recording_max_duration: None,
			transcription_enabled: None,
			tag: None	
		}
	}
}


impl Call{
	pub fn load(&self) -> BResult<()>{
		let path = "users/".to_string() + &self.client.get_user_id() + "/calls/" + &self.id;
		let res:JsonResponse<CallInfo> = try!(self.client.raw_get_request(&path, (), ()));
		let mut data = self.data.lock().unwrap();
		*data = try!(Data::from_info(&res.body));
		Ok(())
	}

	pub fn create(client: &Client, from: &str, to: &str, config: &Config) -> BResult<Call>{
		let path = "users/".to_string() + &client.get_user_id() + "/calls";
		let json = json!({
			"from": (from),
			"to": (to),
			"callTimeout": (config.call_timeout),
			"callbackUrl": (config.callback_url),
			"callbackTimeout": (config.callback_timeout),
			"callbackHttpMethod": (config.callback_http_method),
			"fallbackUrl": (config.fallback_url),
			"bridgeId": (config.bridge_id),
			"conferenceId": (config.conference_id),
			"recordingEnabled": (config.recording_enabled),
			"recordingMaxDuration": (config.recording_max_duration),
			"transcriptionEnabled": (config.transcription_enabled),
			"tag": (config.tag)
		});
		let res:EmptyResponse = try!(client.raw_post_request(&path, (), json));
		let id = try!(util::get_id_from_location_header(&res.headers));
		Ok(Call{
			id: id,
			client: client.clone(),
			data: Arc::new(Mutex::new(Data{
				active_time: NotLoaded,
				bridge_id: NotLoaded,
				callback_url: Available(config.callback_url.clone()),
				direction: Available("out".to_string()),
				from: Available(from.to_string()),
				to: Available(to.to_string()),
				recording_file_format: NotLoaded,
				recording_enabled: NotLoaded,
				start_time: NotLoaded,
				state: NotLoaded,
				transcription_enabled: NotLoaded,
				display_name: NotLoaded,
				preferred_id: NotLoaded,
				withhold_caller_name: NotLoaded,
				withhold_caller_number: NotLoaded
			}))
		})
	}
	pub fn get_calls_from_bridge(bridge: &Bridge) -> BResult<Vec<Call>>{
		let client = bridge.get_client();
		let path = "users/".to_string() + &client.get_user_id() + "/bridges/" + &bridge.get_id() + "/calls";
		let res:JsonResponse<Vec<CallInfo>> = try!(client.raw_get_request(&path, (), ()));
		let mut output = vec!();
		for info in res.body{
			output.push(Call{
				id: info.id.clone(),
				client: client.clone(),
				data: Arc::new(Mutex::new(try!(Data::from_info(&info))))
			});
		}
		Ok(output)
	}
	pub fn from_call_event(event: &CallEvent) -> Call{
		Call{
			id: event.get_call_id(),
			client: event.get_client(),
			data: Arc::new(Mutex::new(Data{
				active_time: NotLoaded,
				bridge_id: NotLoaded,
				callback_url: NotLoaded,
				direction: NotLoaded,
				from: Lazy::load_if_available(event.get_from()),
				to: Lazy::load_if_available(event.get_to()),
				recording_file_format: NotLoaded,
				recording_enabled: NotLoaded,
				start_time: NotLoaded,
				state: NotLoaded,
				transcription_enabled: NotLoaded,
				display_name: Available(event.get_display_name()),
				preferred_id: Available(event.get_preferred_id()),
				withhold_caller_name: Available(event.get_withhold_caller_name()),
				withhold_caller_number: Available(event.get_withhold_caller_number())
			}))
		}
	}
	pub fn add_to_new_bridge(&self, bridge_audio: bool, additional_phone_ids: &Vec<String>) -> BResult<Bridge>{
		let mut calls = additional_phone_ids.clone();
		calls.push(self.get_id());
		Bridge::create(&self.client, bridge_audio, &calls)
	}
	pub fn hang_up(&self) -> BResult<()>{
		let path = "users/".to_string() + &self.client.get_user_id() + "/calls/" + &self.id;
		let _:EmptyResponse = try!(self.client.raw_post_request(&path, (), json!({
			"state": "completed"
		})));
		Ok(())
	}
	
	
	pub fn get_id(&self) -> String{
		self.id.clone()
	}
	pub fn get_client(&self) -> Client{
		self.client.clone()
	}
	pub fn get_active_time(&self) -> BResult<Option<String>>{
		if !self.data.lock().unwrap().active_time.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().active_time.get()).clone())
	}
	pub fn get_bridge_id(&self) -> BResult<Option<String>>{
		if !self.data.lock().unwrap().bridge_id.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().bridge_id.get()).clone())
	}
	pub fn get_callback_url(&self) -> BResult<Option<String>>{
		if !self.data.lock().unwrap().callback_url.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().callback_url.get()).clone())
	}
	pub fn get_direction(&self) -> BResult<String>{
		if !self.data.lock().unwrap().direction.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().direction.get()).clone())
	}
	pub fn get_from(&self) -> BResult<String>{
		if !self.data.lock().unwrap().from.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().from.get()).clone())
	}
	pub fn get_recording_file_format(&self) -> BResult<Option<String>>{
		if !self.data.lock().unwrap().recording_file_format.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().recording_file_format.get()).clone())
	}
	pub fn get_recording_enabled(&self) -> BResult<bool>{
		if !self.data.lock().unwrap().recording_enabled.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().recording_enabled.get()).clone())
	}
	pub fn get_start_time(&self) -> BResult<String>{
		if !self.data.lock().unwrap().start_time.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().start_time.get()).clone())
	}
	pub fn get_state(&self) -> BResult<String>{
		if !self.data.lock().unwrap().state.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().state.get()).clone())
	}
	pub fn get_to(&self) -> BResult<String>{
		if !self.data.lock().unwrap().to.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().to.get()).clone())
	}
	pub fn get_transcription_enabled(&self) -> BResult<bool>{
		if !self.data.lock().unwrap().transcription_enabled.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().transcription_enabled.get()).clone())
	}
	pub fn get_display_name(&self) -> BResult<Option<String>>{
		if !self.data.lock().unwrap().display_name.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().display_name.get()).clone())
	}
	pub fn get_preferred_id(&self) -> BResult<Option<String>>{
		if !self.data.lock().unwrap().preferred_id.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().preferred_id.get()).clone())
	}
	pub fn get_withhold_caller_name(&self) -> BResult<Option<bool>>{
		if !self.data.lock().unwrap().withhold_caller_name.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().withhold_caller_name.get()).clone())
	}
	pub fn get_withhold_caller_number(&self) -> BResult<Option<bool>>{
		if !self.data.lock().unwrap().withhold_caller_number.available(){
			try!(self.load());
		}
		Ok(try!(self.data.lock().unwrap().withhold_caller_number.get()).clone())
	}
	
	pub fn get_bridge(&self) -> BResult<Option<Bridge>>{
		Ok(match try!(self.get_bridge_id()){
			Some(id) => Some(Bridge::get_by_id(&self.client, &id)),
			None => None
		})
	}
}



